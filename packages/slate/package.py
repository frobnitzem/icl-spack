# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class Slate(CMakePackage, CudaPackage):
    """Slate distributed linear algebra library."""

    homepage = "https://www.icl.utk.edu/"
    hg       = "https://bitbucket.org/icl/slate"

    version('develop')

    variant('cuda', default=True, description='Build with CUDA (required)')
    variant('openmp', default=True, description='Compile with OpenMP support')

    depends_on('blaspp')
    depends_on('lapackpp')
    depends_on('mpi')
    depends_on('testsweeper') # for testing

    def cmake_args(self):
        spec = self.spec
        args = [ '-DUSE_OPENMP=%s' % ('ON' if '+openmp' in spec else 'OFF')
               , '-DBUILD_SLATE_TESTS=%s' % ('ON' if self.run_tests else 'OFF')
               ]
        if '+cuda' not in spec:
            raise ValueError("Slate requires +cuda")
            cuda_arch = spec.variants['cuda_arch'].value
            if cuda_arch is not None:
                args.append('-DCUDA_FLAGS=-arch=sm_{0}'.format(cuda_arch[0]))
        #else:
        #    args.append('-DWITH_CUDA=OFF')

        return args

    @run_after('build')
    @on_package_attributes(run_tests=True)
    def check(self):
        """Run ctest after building project."""

        with working_dir(self.build_directory):
            ctest(parallel=False)


# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class Lapackpp(CMakePackage):
    """C++ Wrappers for LAPACK."""

    homepage = "https://www.icl.utk.edu/"
    hg       = "https://bitbucket.org/icl/lapackpp"

    version('develop')

    variant('openmp', default=True, description='Compile with OpenMP support')
    #variant('ilp64',  default=False, description='Use 64 bit integers')
    #variant('threads', default='none', description='Multithreading strategy of underlying blas',
    #                   values=('pthreads', 'openmp', 'none'), multi=False
    #       )
    variant('blas', default='openblas', description='Underlying blas library',
                    values=('openblas', 'apple-accelerate', 'cray-libsci', 'essl', 'intel-mkl'), multi=False
           )

    depends_on('openblas@0.3.0:', when='blas=openblas') # older versions use float* for complex, causing conversion error
    depends_on('cray-libsci', when='blas=cray-libsci')
    depends_on('essl',        when='blas=essl')
    depends_on('intel-mkl',   when='blas=intel-mkl')
    depends_on('blaspp')

    depends_on('testsweeper') # for testing

    def cmake_args(self):
        spec = self.spec
        library_id = {
                "openblas"         : "OpenBLAS",
                "apple-accelerate" : "Apple Accelerate",
                "cray-libsci"      : "Cray LibSci",
                "essl"             : "IBM ESSL",
                "intel-mkl"        : "Intel MKL"
        }
        if 'blas=apple-accelerate' in spec:
            threaded = True # FIXME: Apple's documentation doesn't say...
            int64 = False
            extra_libs = []
        else:
            blas = spec[spec.variants['blas'].value]
            threaded = 'threads=none' not in blas
            int64 = '+ilp64' in blas
            extra_libs = ['-DCMAKE_LIBRARY_PATH=%s' % ';'.join(blas.libs.directories) ]

        args = [ '-DBLAS_LIBRARY="%s"' % library_id[spec.variants['blas'].value]
               , '-DUSE_OPENMP=%s' % ('ON' if '+openmp' in spec else 'OFF')
               , '-DBUILD_LAPACKPP_TESTS=%s' % ('ON' if self.run_tests else 'OFF')
               , '-DBLAS_LIBRARY_INTEGER="%s"' % ("int64_t (ILP64)" if int64 else "int (LP64)")
               , '-DBLAS_LIBRARY_THREADING=%s' % ("threaded" if threaded else "sequential")
               ] + extra_libs

        return args

    @run_after('build')
    @on_package_attributes(run_tests=True)
    def check(self):
        """Run ctest after building project."""

        with working_dir(self.build_directory):
            ctest(parallel=False)


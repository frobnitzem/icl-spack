# Copyright 2013-2020 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *

class Testsweeper(CMakePackage):
    """Testing framework for matrix algorithms to automate running on multiple input sizes and precisions."""

    homepage = "https://www.icl.utk.edu/"
    hg       = "https://bitbucket.org/icl/testsweeper"

    version('develop')

    def configure_args(self):
        spec = self.spec
        args = []
        if self.run_tests:
            args += ['-DBUILD_TESTS=ON']
        else:
            args += ['-DBUILD_TESTS=OFF']
        return args

    @run_after('build')
    @on_package_attributes(run_tests=True)
    def check(self):
        """Run ctest after building project."""

        with working_dir(self.build_directory):
            ctest(parallel=False)


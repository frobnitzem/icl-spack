= Innovative Computing Laboratory Spack Repository

This repository will be the most up to date location
for Spack packages for Slate, and related dependency projects
Blas++ and Lapack++.

== Getting Started

First, make sure [Spack](https://github.com/spack/spack) is cloned
in any directory -- say `$SPACK_ROOT`.
Then, set up your environment to use it:

```bash
source $SPACK_ROOT/share/spack/setup-env.sh
```

Download this repository and run,

```bash
spack repo add icl-spack
```

To validate that Spack sees the new repo, run:

```bash
spack repo list
```

You can display information about how to install the packages with, for example:

```
spack info slate
```

Installing slate should automatically trigger installation
of its dependencies with compatible options.


== Setting Up Spack: Avoiding the Package Cascade

By default, Spack doesn't 'see' anything on your system - including things like CMake and CUDA.
At minimum, we recommend adding a `packages.yaml` to your `$HOME/.spack` folder that includes CMake
(and CUDA, if applicable). For example, your `$HOME/.spack/packages.yaml` file could be:

```yaml
packages:
 cuda:
  modules: # for HPC systems with modules for loading software
   cuda@9.2.88: [cuda/9.2.88]
  #paths: # manual paths for local systems
  # cuda@9.2.88:
  #  /usr/local/cuda
  buildable: false
 cmake:
  modules:
   cmake: [cmake]
  #paths:
  # cmake:
  #  /usr
  buildable: false
```

The modules entry is only necessary on systems that require loading Modules (i.e. most high performance compute clusters).
The buildable flag is useful to make sure Spack crashes if there is a path error, rather than having a typo
and Spack rebuilding everything because cmake isn't found.
You can verify your environment is set up correctly by running spack spec.
For example:

```bash
spack spec -I slate +cuda
```

should show a tree of packages on which slate depends.
Each is annotated as either currently installed ([+]) or to be installed (-).

== Use With Testing

A Spack 'spec' provides a convenient way to define testing configurations for a CI infrastructure like Jenkins. A spec such as:

`slate@develop +cuda`

defines a particular build of Slate that you want checked consistently on the develop branch.
By default, testing is not activated. To have Spack both build and test a given spec, run:

spack install --test=root slate@develop +cuda

Here --test=root says to run tests on the 'root' package (not the dependent packages),
which in this case is slate itself and not Lapack++, etc.

